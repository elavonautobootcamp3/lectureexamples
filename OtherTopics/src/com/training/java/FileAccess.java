package com.training.java;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

class MyClass implements AutoCloseable {

	@Override
	public void close() throws Exception {
		System.out.println("close method of my class");
		
	}
		
}
public class FileAccess {

	public static void main(String[] args) {
		
		System.out.println("this\n\tis\n\\sentence");
		
		Path path = Paths.get("C:/", "Users", "rapasive", "myFile.txt");
		
//		System.out.println(Files.exists(Paths.get("dfgjlwfkgjlwgj")));
		
		Path path2 = Paths.get("C:\\Users\\..\\rapasive\\..\\myFile.txt");
		
		System.out.println("path2: " + path2.normalize());
		
		Path newPath = Paths.get("C:/", "Users", "rapasive", "myNewFile.txt");

		try (BufferedReader reader = Files.newBufferedReader(path);
			BufferedWriter writer = Files.newBufferedWriter(newPath, StandardOpenOption.CREATE, StandardOpenOption.APPEND);
				MyClass myClass = new MyClass()) {
			
			String str = null;

			while ((str = reader.readLine()) != null) {
				writer.write(str);
				writer.newLine();
//				System.out.println(str);
			}
			
		} catch (Exception e){
			
		} finally {
			
		}
		
			
	}

}
