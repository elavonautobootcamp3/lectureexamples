package com.training.java;

import java.math.BigInteger;
import java.util.ArrayDeque;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;


public class Queues {
	
	public static void maina(String[] args) {
		
		/*
		int i, j;
		
		for(i=1, j=0; i < 10; i++){
			j += i;
		}
		
		System.out.println(i);
		*/
		
		/*
		int x = 2;
		int y = 0;
		
		for(;y<10; ++y){
			if(y%x == 0)
				continue;
			else if (y == 8)
				break;
			else
				System.out.print(y + " ");
		}
		*/
		/*
		 int x = 0; // Line 1
		while(1)//line 6
		{
			System.out.println("x plus one is " + (x + 1)); //line 8
		 
		}
		*/
		/*
		 int odd = 1;
		 if(odd){
		 	System.out.println("odd");
		 } else {
		 	System.out.println("even");
		 }
		 */
		 
		 /*
		  int number =3;
		  if(number >= 0){
		  	if(number == 0)
		  		System.out.println("A");
		  	}
		  else {
		  		System.out.println("B");
		  	}
		  	System.out.println("C");
		  */
		
		/*
		boolean condition = false;
		
		do {
			System.out.println("Hello");
		} while(condition);
		
		while(condition){
			
		}
		*/
		/*
		byte b= 1;
		
		switch(b){
		
		}
		*/
		
	}
	

	public static void main(String[] args) {

		Queue<String> q = new LinkedList<String>();
		
		q.offer("1");
		q.offer("2");
		q.add("3");
		q.add("4");
		q.offer("5");
		
//		q.forEach(System.out::println);

		ArrayDeque<String> ad = new ArrayDeque<String>();
		
		ad.offer("1");
		ad.offer("2");
		ad.add("3");
		ad.add("4");
		ad.offer("5");
		ad.push("6");
//		ad.pop();
		
		ad.forEach(System.out::println);
		
		Stack<String> st = new Stack<>();
		
		st.push("1");
		st.push("2");
		st.push("3");
		st.push("4");
		st.push("5");
		
		//st.forEach(System.out::println);

//		for(int i = 0; i < st.size(); i++){
//			System.out.println(st.pop());
//		}
		
//		while(!st.isEmpty()){
//			System.out.println(st.pop());
//		}
		
	}

}
