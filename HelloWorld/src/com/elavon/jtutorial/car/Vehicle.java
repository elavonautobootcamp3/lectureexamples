package com.elavon.jtutorial.car;

public class Vehicle {
	public double speed = 5;
	
	void displaySpeed() {
		System.out.println(speed);
	}
	
	public static void main(String[] args) {
		Vehicle v = new Vehicle();
		v.displaySpeed();
	}
}
