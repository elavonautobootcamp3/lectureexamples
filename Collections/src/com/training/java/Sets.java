package com.training.java;

import java.util.HashSet;
import java.util.List;
import java.util.Queue;
import java.util.Set;

public class Sets {

	public static void main(String[] args) {
		Set<String> set = new HashSet<>();

		set.add("The");
		set.add("quick");
		set.add("brown");
		set.add("fox");
		set.add("fox");
		
		displaySet(set);
	}

	static void displaySet(Set<String> set) {
		System.out.println();
		
		for (String str : set) {
			System.out.print(str + " ");
		}
	}
}
