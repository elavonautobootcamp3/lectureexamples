package com.training.java;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Lists {

	public static void main(String[] args) {

		List<String> list = new ArrayList<>();

		list.add("the");
		list.add("quick");
		list.add("broooown");
		list.add("foxees");

		// displayList(list);
		//
		// list.get(2);
		//
		// displayList(list);
		//
		// list.remove(2);
		//
		// displayList(list);
		//
		// list.set(2, "black" );
		//
		// System.out.println(list.contains("quick"));

		// List<Integer> ints = new ArrayList<>();
		//
		// ints.add(1);
		// ints.add(34);
		// ints.add(22);
		// ints.add(77);
		// ints.add(11);

		Comparator<String> myComp = new MyComparator();
		
		Collections.sort(list, myComp);
		
		Integer[] is = new Integer[]{45, 64,345,3,53,2};
		
		Arrays.sort(is, Collections.reverseOrder());
		
		for(int i : is){
			System.out.println(i);
		}
		
		// System.out.println(ints);
		// int[] arr = new int[]{5,465,764,3,6,3,6,78};
		//
		// Arrays.sort(arr);
		//
		// for(int i : arr){
		//
		// System.out.print(i + " ");
		// }

//		displayList(list);
	}

	

	static void displayList(List<String> list) {
		System.out.println();
		for (String str : list) {
			System.out.print(str + " ");
		}
	}

}
