package com.training.java;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

class Animal {
}

public class Maps {

	public static void main(String[] args) {
		
		Map<String, Object> map = new HashMap<>();

		map.put("Rj", "1923");
		map.put("JREY", "1955");
		map.put("Janjan", "1869");
		map.put("Chris", "4562");
		map.put("animal", new Animal());
		map.put("Chris", "3452");
		
//		System.out.println(map);
//		
//		System.out.println(map.get("JREY"));
		
		Set<String> set = map.keySet();
		
		for(String key : set){
			System.out.println(map.get(key));
		}
		
		System.out.println("Map : " + map.size());
	}

}
