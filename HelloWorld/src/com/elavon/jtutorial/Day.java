package com.elavon.jtutorial;

public enum Day {
    SUNDAY("Linggo"), MONDAY("Lunes"), TUESDAY("Martes"), WEDNESDAY("Miyerkules"),
    THURSDAY("Huwebes"), FRIDAY("Biyernes"), SATURDAY("Sabado") ;
    
    String filipino;
    
    Day(String filipino) {
        this.filipino = filipino;
    }
    
    String inFilipino() {
        return this.filipino;
    }
    
    public static void main(String[] args) {
        System.out.println(Day.SUNDAY);
        System.out.println(Day.MONDAY.inFilipino());
        Day expirationDay = Day.TUESDAY;
        
        
        System.out.println(Day.TUESDAY.equals(expirationDay));
//        System.out.println(Day.WEDNESDAY < Day.THURSDAY);
    }
}
