package com.training.java;

import javax.swing.JOptionPane;

import static javax.swing.JOptionPane.YES_OPTION;
import static javax.swing.JOptionPane.NO_OPTION;
import static javax.swing.JOptionPane.CANCEL_OPTION;

public class Gooey {

	public static void main(String[] args) {

		// String name = JOptionPane.showInputDialog(null, "Enter name",
		// "Registration", JOptionPane.ERROR_MESSAGE);
		//
		// JOptionPane.showMessageDialog(null, "Are you sure?", "Question",
		// JOptionPane.QUESTION_MESSAGE);

		int i = JOptionPane.showConfirmDialog(null, "Confirm?");

		String[] arr = new String[]{ "1", "2", "3", "asd", "qwe", "Hello"};
		
		int index = JOptionPane.showOptionDialog(null,
				"message",
				"Select one",
				JOptionPane.DEFAULT_OPTION,
				JOptionPane.INFORMATION_MESSAGE,
				null,
				arr,
				"1");
		
		System.out.println(arr[index]);
		
		switch (i) {
		case YES_OPTION:
			System.out.println("YES");
			break;
		case NO_OPTION:
			System.out.println("NO");
			break;
		case CANCEL_OPTION:
			System.out.println("CANCEL");
			break;
		default:
			System.out.println(i);
		}
	}

}
