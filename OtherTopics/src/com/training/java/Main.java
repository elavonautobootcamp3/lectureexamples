package com.training.java;

class Animal {
	void makeSound(){
		System.out.println("animal sound");
	}
}

class Dog extends Animal {
	void stay(){
		
	}
	void makeSound(){
		System.out.println("bark");
	}
}

class Cat extends Animal {
	void makeSound(){
		System.out.println("meow");
	}
}

class Pug extends Dog {}

class Hound extends Dog {}

class Bengal extends Cat {}

public class Main {

	public static void main(String[] args) {
		
		Animal animal = new Dog();
		animal.makeSound();
		
		Dog dog = (Dog) animal; // downcasting
		dog.makeSound();

//		dog.stay();
//		dog.makeSound();
//		
//		Dog dog1 = new Pug();
//		
//		Pug p = (Pug) dog1;
//		
//		
//		Bengal bengal = new Bengal();
//		Cat cat = (Cat) bengal; // upcasting
//
//		Bengal b1 = (Bengal) cat;
//		
//		Hound hound = (Hound) new Hound(); // unnecessary casting
//
//		method1((Dog)animal);
	}
	
	static void method1(Dog animal){
		
		animal.makeSound();
		
	}

}
